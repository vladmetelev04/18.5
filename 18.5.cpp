﻿#include <iostream>
#include <string>
using namespace std;

class player
{
private:
    
    string PlayerName;
    int PlayerScore;


public:
    player() : PlayerName("Un"), PlayerScore(0)
    {}
    
    player(string _PlayerName, int _PlayerScore) :
        PlayerName(_PlayerName), PlayerScore(_PlayerScore)
    {}

    void Show()
    {

        cout << "\n" << PlayerName << " : " << PlayerScore;
    }

    int getScorePlayer()

    {

        return PlayerScore;

    }

    void setScorePlayer(int NewScorePlayer)
    {

        PlayerScore = NewScorePlayer;
    }

    string getPlayerName()
    {
        return PlayerName;
    }

    void setPlayerName(string newPlayerName, int newPlayerScore)
    {

        PlayerName = newPlayerName;
        PlayerScore = newPlayerScore;

    }

};


void SelectionSort(player* base, int size)
{

    int j;
    player tmp;
    for (int i = 0; i < size; i++)
    {
        j = i;
        for (int k = i; k < size; k++)
        {
            if (base[j].getScorePlayer() > base[k].getScorePlayer())
            {
                j = k;
            }


        }
        tmp = base[i];
        base[i] = base[j];
        base[j] = tmp;
    }
}

int main()
{

    cout << "How Many Players Do You Want?\n";

    int size;
    cin >> size;

    string arrName;
    int arrScore;

    player* base = new player[size];


    for (int i = 0, j = 0; i < size; i++, j++)
    {
        string y;
        int x;

        cout << "Enter " << i + 1 << " name:";
        cin >> y;
        arrName = y;

        cout << "Enter " << y << " score:";
        cin >> x;
        arrScore = x;

        base[i].setPlayerName(arrName, arrScore);
    }

    cout << "\nYou Enter: \n";
    for (int i = 0; i < size; i++)
    {
        base[i].Show();
    }

    SelectionSort(base, size);
    cout << endl;


    for (int i = 0; i < size; i++)
    {

        base[i].Show();
    }


    return 0;
}

